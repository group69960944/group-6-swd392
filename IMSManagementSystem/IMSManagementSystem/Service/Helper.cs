﻿using CloudinaryDotNet.Actions;
using CloudinaryDotNet;

namespace IMSManagementSystem.Service
{
    public class Helper
    {
        private static string CLOUD_NAME = "dzms1mqdb";
        private static string API_KEY = "266943952967659";
        private static string API_SECRET = "FiL6-oRvuux1OgSKjDTI9Zjfd9A";

        public static string UploadPhoto(Stream stream)
        {
            Account account = new Account(
             CLOUD_NAME,
              API_KEY,
             API_SECRET);

            Cloudinary cloudinary = new Cloudinary(account);
            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(Guid.NewGuid().ToString(), stream),
            };
            ImageUploadResult uploadResult = cloudinary.Upload(uploadParams);
            return cloudinary.Api.UrlImgUp.BuildUrl(string.Format("{0}.{1}", uploadResult.PublicId, uploadResult.Format));
        }
    }
}
