﻿using IMSManagementSystem.Dao;
using IMSManagementSystem.Models;

namespace IMSManagementSystem.Service
{
    public class MilestoneService
    {
        public MilestoneDao milestoneDao;
        public MilestoneService() {
            milestoneDao = new MilestoneDao();
        }
        public List<Milestone> GetMilestoneLst()
        {
            return milestoneDao.GetMilestoneLst();
        }
        public Milestone GetMilestoneById(int id)
        {
            return milestoneDao.GetMilestoneById(id);
        }
    }
}
