﻿using IMSManagementSystem.Commons;
using IMSManagementSystem.Model;
using IMSManagementSystem.Models;
using System.Collections.Generic;
namespace IMSManagementSystem.Services
{
    public class BaseService
    {
        public bool IsNullOrEmpty<T>(T value)
        {
            if (value == null)
                return true;
            return false;
        }
        public Pager<T> PagingConfig<T>(int pg,List<T> lst)
        {
            var pager = new Pager<T>();
            if (pg < 1) pg = 1;
            int pageSize = Constant.pageSize;
            int count = lst.Count();
            pager = new Pager<T>(count, pg, pageSize);
            int skip = (pg - 1) * pageSize;
            var data = lst.Skip(skip).Take(pager.PageSize).ToList();
            pager.Data = data;
            return pager;
        }
        public bool ValidateEmail(string email) { return true; }
        public bool ValidatePhone(int phone) { return true; }
    }
}
