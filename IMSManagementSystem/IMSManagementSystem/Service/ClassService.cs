﻿using IMSManagementSystem.Dao;
using IMSManagementSystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IMSManagementSystem.Services
{
    public class ClassService : BaseService
    {
        private ClassDao classDao;
        public ClassService() {
            classDao = new ClassDao();
        }
        public List<Class> GetList(string? className)
        { 
            return classDao.GetList(className);
        }
        public void DeleteClass(int id)
        {
            classDao.DeleteClass(id);
        }
        public List<SelectListItem> GetSemesterOptions()
        {
            return classDao.GetSemesterOptions();
        }
        public List<SelectListItem> GetSubjectOptions()
        {
            return classDao.GetSubjectOptions();
        }
        public List<SelectListItem> GetTeacherOptions()
        {
            return classDao.GetTeacherOptions();
        }
        public Class GetClassById(int id)
        {
            return classDao.GetClassById(id);
        }
        public List<ClassStudent> GetClassStudent(int id)
        {
            return classDao.GetClassStudent(id);
        }
        public List<Milestone> GetClassMileStone(int id)
        {
            return classDao.GetClassMileStone(id);
        }
        public List<IssueSetting> GetClassIssueSetting(int id)
        {
            return classDao.GetClassIssueSetting(id);
        }
        public void EditClass(Class clsRaw)
        {
            classDao.EditClass(clsRaw);
        }
        public void AddClass(Class cls)
        {
            classDao.AddClass(cls);
        }
        public bool CheckClassValid(Class cls)
        {
            if(IsNullOrEmpty(cls.ClassName) 
                || IsNullOrEmpty(cls.SubjectId)
                || IsNullOrEmpty(cls.SemesterId)
                || IsNullOrEmpty(cls.TeacherId)
                || IsNullOrEmpty(cls.StartedDate)
                || IsNullOrEmpty(cls.IsPending)
                ) 
                return false;
            return true;
        }
        public bool IsDuplicateClass(Class cls)
        {
            Class clsCheck = classDao.Classes.FirstOrDefault(x => x.ClassName == cls.ClassName && x.TeacherId == cls.TeacherId);
            if(clsCheck == null) return false;
            return true;
        }
    }
}
