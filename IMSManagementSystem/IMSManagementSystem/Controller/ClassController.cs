﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IMSManagementSystem.Dao;
using IMSManagementSystem.Models;
using IMSManagementSystem.Services;
using System.Security.Claims;
using IMSManagementSystem.Model;
using IMSManagementSystem.Commons;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace IMSManagementSystem.Controller
{
    public class ClassController : Microsoft.AspNetCore.Mvc.Controller
    {
        private ClassService classService;
        public ClassController()
        {
            classService = new ClassService();
        }

        // GET: Class
        public async Task<IActionResult> List(string? className,int pg=1)
        {
            var lstClass = classService.GetList(className);
            var pager = classService.PagingConfig<Class>(pg, lstClass);
            this.ViewBag.Pager = pager;
            this.ViewBag.ClassName = className;
            ViewBag.isSuccess = TempData["isSuccess"];
            return View(pager.Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult List(int cId)
        {
            classService.DeleteClass(cId);
            TempData["isSuccess"] = true;
            string className = HttpContext.Request.Query["className"];
            int pg = Convert.ToInt32(HttpContext.Request.Query["pg"]);
            return RedirectToAction("List", new { className = className, pg = pg, isSuccess = true });

        }

        // GET: Class/Create
        public IActionResult Create()
        {
            ViewData["Semester"] = new SelectList(classService.GetSemesterOptions(), "Value", "Text");
            ViewData["Subject"] =  new SelectList(classService.GetSubjectOptions(), "Value", "Text");
            ViewData["Teacher"] =  new SelectList(classService.GetTeacherOptions(), "Value", "Text");
            ViewData["isSuccess"] = false;
            ViewData["isDuplicate"] = false;
            return View();
        }

        // POST: Class/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClassName,SubjectId,SemesterId,TeacherId,StartedDate,IsPending")] Class @class)
        {
            ViewData["isSuccess"] = false;
            ViewData["isDuplicate"] = false;
            if (classService.CheckClassValid(@class))
            {
                if (classService.IsDuplicateClass(@class))
                {
                    ViewData["isDuplicate"] = true;
                }
                else
                {
                    classService.AddClass(@class);
                    ViewData["isSuccess"] = true;
                }

            }
            ViewData["Semester"] = new SelectList(classService.GetSemesterOptions(), "Value", "Text", @class.SemesterId);
            ViewData["Subject"] = new SelectList(classService.GetSubjectOptions(), "Value", "Text", @class.SubjectId);
            ViewData["Teacher"] = new SelectList(classService.GetTeacherOptions(), "Value", "Text", @class.TeacherId);
            return View(@class);
        }

        //    // GET: Class/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            Class cls = new Class();
            if (id != null)
            {
                cls = classService.GetClassById(id);
            }
            List<ClassStudent> student = classService.GetClassStudent(id);
            ViewData["Student"] = student;
            ViewData["Milestone"] = classService.GetClassMileStone(id);
            ViewData["Setting"] = classService.GetClassIssueSetting(id);
            ViewData["Semester"] = new SelectList(classService.GetSemesterOptions(), "Value", "Text", cls.SemesterId);
            ViewData["Subject"] = new SelectList(classService.GetSubjectOptions(), "Value", "Text", cls.SubjectId);
            ViewData["Teacher"] = new SelectList(classService.GetTeacherOptions(), "Value", "Text", cls.TeacherId);
            ViewData["isSuccess"] = false;
            return View(cls);
        }

        // POST: Class/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClassId,ClassName,SubjectId,SemesterId,TeacherId,StartedDate,IsPending")] Class @class)
        {
            if (classService.CheckClassValid(@class))
            {
                classService.EditClass(@class);
                ViewData["isSuccess"] = true;
            }
            else
            {
                ViewData["isSuccess"] = false;
            }
            List<ClassStudent> student = classService.GetClassStudent(id);
            ViewData["Student"] = student;
            ViewData["Milestone"] = classService.GetClassMileStone(id);
            ViewData["Setting"] = classService.GetClassIssueSetting(id);
            ViewData["Semester"] = new SelectList(classService.GetSemesterOptions(), "Value", "Text", @class.SemesterId);
            ViewData["Subject"] = new SelectList(classService.GetSubjectOptions(), "Value", "Text", @class.SubjectId);
            ViewData["Teacher"] = new SelectList(classService.GetTeacherOptions(), "Value", "Text", @class.TeacherId);
            return View();
        }
    }
}
