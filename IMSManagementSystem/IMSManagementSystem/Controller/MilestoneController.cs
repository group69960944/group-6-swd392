﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IMSManagementSystem.Dao;
using IMSManagementSystem.Models;
using IMSManagementSystem.Service;

namespace IMSManagementSystem.Controller
{
    public class MilestoneController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly MilestoneService milestoneService;

        public MilestoneController()
        {
            milestoneService = new MilestoneService();
        }

        // GET: Milestone
        public async Task<IActionResult> List()
        {
            return View(milestoneService.GetMilestoneLst());
        }

        // GET: Milestone/Details/5
        public IActionResult Detail(int id)
        {
            return PartialView("Detail", milestoneService.GetMilestoneById(id));
        }

        
    }
}
