﻿using IMSManagementSystem.Dao;
using IMSManagementSystem.Models;
using IMSManagementSystem.Service;
using Microsoft.AspNetCore.Mvc;
using MySqlX.XDevAPI;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace IMSManagementSystem.Controller
{
    public class LoginController : Microsoft.AspNetCore.Mvc.Controller
    {
        private BaseDao baseDao;

        public LoginController()
        {
            baseDao = new BaseDao();
        }

        [HttpGet]
        [Route("/")]
        public IActionResult Index()
        {

            return View("~/Views/Login/Login.cshtml");
        }
        [HttpGet]
        [Route("/logout")]
        public IActionResult Logout()
        {

            return View("~/Views/Login/Login.cshtml");
        }

        [HttpPost]
        [Route("login")]
        public IActionResult login(string username, string password)
        {
            User user = baseDao.Users.Where(n => n.Username == username && n.Password == password).FirstOrDefault();
            if (user == null)
            {
                ViewBag.error = "Invalid Username or Password";
                return Index();
            }
            else
            {
                HttpContext.Session.SetString("User", JsonSerializer.Serialize(user));
                return Redirect("/class/list");
            }
        }

        [Route("register")]
        public IActionResult IndexRegister()
        {
            return View("~/Views/Login/Register.cshtml");
        }

        public IActionResult Register(string username, string password, string fullname, string email, string phone, IFormFile file)
        {
            User user = baseDao.Users.Where(n => n.Username == username || n.Gmail == email).FirstOrDefault();
            if (user != null)
            {
                ViewBag.error = "Invalid username or email. Username or Email existed!";
                return IndexRegister();
            }
            user = new User
            {
                Username = username,
                Password = password,
                FullName = fullname,
                Gmail = email,
                Phone = 1,
                RoleId = 1,
                Role = baseDao.SystemSettings.SingleOrDefault(n => n.SettingId == 1),
                IsActive = true,
                Avatar = Helper.UploadPhoto(file.OpenReadStream())
            };
            baseDao.Users.Add(user);
            baseDao.SaveChanges();
            return Index();
        }
    }
}
