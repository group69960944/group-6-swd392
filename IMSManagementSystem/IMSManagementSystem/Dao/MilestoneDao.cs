﻿using IMSManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace IMSManagementSystem.Dao
{
    public class MilestoneDao: BaseDao
    {
        public List<Milestone> GetMilestoneLst()
        {
            return Milestones.Include(m => m.Class).Include(m => m.Project).ToList();
        }
        public Milestone GetMilestoneById(int id)
        {
            return Milestones.Include(m => m.Class).Include(m => m.Project).FirstOrDefault(x => x.MilestoneId == id);
        }
    }
}
