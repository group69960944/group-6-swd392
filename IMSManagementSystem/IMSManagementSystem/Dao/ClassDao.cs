﻿using IMSManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace IMSManagementSystem.Dao
{
    public class ClassDao : BaseDao
    {
        public ClassDao() {
        }
        public List<Class> GetList(string? className)
        {
            List<Class> lst = new List<Class>();
            if(!string.IsNullOrEmpty(className))
            {
                lst = Classes.Include(x => x.Teacher)
                          .Include(c => c.Semester)
                          .Include(b => b.Subject)
                          .Where(x => x.ClassName.Contains(className))  
                          .ToList();
            }
            else
            {
                lst = Classes.Include(x => x.Teacher)
                          .Include(c => c.Semester)
                          .Include(b => b.Subject).ToList();
            }
            return lst;
        }
        public void AddClass(Class cls)
        {
            cls.CreatedDate= DateTime.Now;
            Classes.Add(cls);
            SaveChanges();
        }

        public void DeleteClass(int id)
        {
            Class cls = GetClassById(id);
            if (cls != null)
            {
                if (cls.IsPending == true)
                {
                    List <ClassStudent> classStudentLst = ClassStudents.Where(x => x.ClassId == cls.ClassId).ToList();
                    List <Project> projectLst = Projects.Where(x => x.ClassId == cls.ClassId).ToList();
                    List<Milestone> milestoneLst = Milestones.Where(x => x.ClassId == cls.ClassId).ToList();
                    ClassStudents.RemoveRange(classStudentLst);
                    Projects.RemoveRange(projectLst);
                    Milestones.RemoveRange(milestoneLst);
                    Classes.Remove(cls);
                }
                else
                {
                    cls.IsPending = true;
                    Classes.Update(cls);
                    SaveChanges();
                }
                SaveChanges();
            }
        }
        public void EditClass(Class clsRaw)
        {
            Class cls = GetClassById(clsRaw.ClassId);
            if (cls != null)
            {
                cls.ClassName= clsRaw.ClassName;
                cls.TeacherId= clsRaw.TeacherId;
                cls.SemesterId= clsRaw.SemesterId;
                cls.SubjectId = clsRaw.SubjectId;
                cls.IsPending= clsRaw.IsPending;
                cls.UpdatedDate= DateTime.Now;
                Classes.Update(cls);
                SaveChanges();
            }
        }

        public void CancelStartedClass(int id)
        {

        }
        public Class GetClassById(int id)
        {
            return Classes.FirstOrDefault(x => x.ClassId == id);
        }
        public List<ClassStudent> GetClassStudent(int id)
        {
            return ClassStudents
                .Include(x => x.Student)
                .Where(x => x.ClassId == id)
                .ToList();
        }
        public List<Milestone> GetClassMileStone(int id)
        {
            return Milestones.Where(x => x.ClassId == id).ToList();
        } 

        public List<IssueSetting> GetClassIssueSetting(int id)
        {
            return IssueSettings.ToList();
        }
    }
}
