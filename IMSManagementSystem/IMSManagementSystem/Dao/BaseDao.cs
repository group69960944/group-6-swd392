﻿using System;
using System.Collections.Generic;
using IMSManagementSystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IMSManagementSystem.Dao;

public partial class BaseDao : DbContext
{
    public BaseDao()
    {
    }

    public BaseDao(DbContextOptions<BaseDao> options)
        : base(options)
    {
    }

    public virtual DbSet<Class> Classes { get; set; }

    public virtual DbSet<ClassStudent> ClassStudents { get; set; }

    public virtual DbSet<Issue> Issues { get; set; }

    public virtual DbSet<IssueSetting> IssueSettings { get; set; }

    public virtual DbSet<Milestone> Milestones { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    public virtual DbSet<ProjectMember> ProjectMembers { get; set; }

    public virtual DbSet<Subject> Subjects { get; set; }

    public virtual DbSet<SystemSetting> SystemSettings { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL(new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetConnectionString("myDB"));

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Class>(entity =>
        {
            entity.HasKey(e => e.ClassId).HasName("PRIMARY");

            entity.ToTable("class");

            entity.HasIndex(e => e.SemesterId, "semester_id");

            entity.HasIndex(e => e.SubjectId, "subject_id");

            entity.HasIndex(e => e.TeacherId, "teacher_id");

            entity.Property(e => e.ClassId)
                .HasColumnType("int(11)")
                .HasColumnName("class_id");
            entity.Property(e => e.ClassName)
                .HasMaxLength(30)
                .HasColumnName("class_name");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("date")
                .HasColumnName("created_date");
            entity.Property(e => e.IsPending).HasColumnName("is_pending");
            entity.Property(e => e.SemesterId)
                .HasColumnType("int(11)")
                .HasColumnName("semester_id");
            entity.Property(e => e.StartedDate)
                .HasColumnType("date")
                .HasColumnName("started_date");
            entity.Property(e => e.SubjectId)
                .HasColumnType("int(11)")
                .HasColumnName("subject_id");
            entity.Property(e => e.TeacherId)
                .HasColumnType("int(11)")
                .HasColumnName("teacher_id");
            entity.Property(e => e.UpdatedDate)
                .HasColumnType("date")
                .HasColumnName("updated_date");

            entity.HasOne(d => d.Semester).WithMany(p => p.Classes)
                .HasForeignKey(d => d.SemesterId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("class_ibfk_2");

            entity.HasOne(d => d.Subject).WithMany(p => p.Classes)
                .HasForeignKey(d => d.SubjectId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("class_ibfk_1");

            entity.HasOne(d => d.Teacher).WithMany(p => p.Classes)
                .HasForeignKey(d => d.TeacherId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("class_ibfk_3");
        });

        modelBuilder.Entity<ClassStudent>(entity =>
        {
            entity.HasKey(e => new { e.ClassId, e.StudentId }).HasName("PRIMARY");

            entity.ToTable("class_student");

            entity.HasIndex(e => e.StudentId, "student_id");

            entity.Property(e => e.ClassId)
                .ValueGeneratedOnAdd()
                .HasColumnType("int(11)")
                .HasColumnName("class_id");
            entity.Property(e => e.StudentId)
                .HasColumnType("int(11)")
                .HasColumnName("student_id");
            entity.Property(e => e.IsActive).HasColumnName("is_active");

            entity.HasOne(d => d.Class).WithMany(p => p.ClassStudents)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("class_student_ibfk_2");

            entity.HasOne(d => d.Student).WithMany(p => p.ClassStudents)
                .HasForeignKey(d => d.StudentId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("class_student_ibfk_1");
        });

        modelBuilder.Entity<Issue>(entity =>
        {
            entity.HasKey(e => e.IssueId).HasName("PRIMARY");

            entity.ToTable("issue");

            entity.HasIndex(e => e.AssigneeId, "assignee_id");

            entity.HasIndex(e => e.AssignerId, "assigner_id");

            entity.HasIndex(e => e.ProjectId, "project_id");

            entity.Property(e => e.IssueId)
                .HasColumnType("int(11)")
                .HasColumnName("issue_id");
            entity.Property(e => e.AssigneeId)
                .HasColumnType("int(11)")
                .HasColumnName("assignee_id");
            entity.Property(e => e.AssignerId)
                .HasColumnType("int(11)")
                .HasColumnName("assigner_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("date")
                .HasColumnName("created_date");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.ProjectId)
                .HasColumnType("int(11)")
                .HasColumnName("project_id");
            entity.Property(e => e.Title)
                .HasMaxLength(50)
                .HasColumnName("title");
            entity.Property(e => e.TypeId)
                .HasColumnType("int(11)")
                .HasColumnName("type_id");
            entity.Property(e => e.UpdatedDate)
                .HasColumnType("date")
                .HasColumnName("updated_date");

            entity.HasOne(d => d.Assignee).WithMany(p => p.IssueAssignees)
                .HasForeignKey(d => d.AssigneeId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("issue_ibfk_2");

            entity.HasOne(d => d.Assigner).WithMany(p => p.IssueAssigners)
                .HasForeignKey(d => d.AssignerId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("issue_ibfk_1");

            entity.HasOne(d => d.Project).WithMany(p => p.Issues)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("issue_ibfk_3");
        });

        modelBuilder.Entity<IssueSetting>(entity =>
        {
            entity.HasKey(e => e.SettingId).HasName("PRIMARY");

            entity.ToTable("issue_setting");

            entity.Property(e => e.SettingId)
                .HasColumnType("int(11)")
                .HasColumnName("setting_id");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.SettingName)
                .HasMaxLength(50)
                .HasColumnName("setting_name");
        });

        modelBuilder.Entity<Milestone>(entity =>
        {
            entity.HasKey(e => e.MilestoneId).HasName("PRIMARY");

            entity.ToTable("milestone");

            entity.HasIndex(e => e.ClassId, "class_id");

            entity.HasIndex(e => e.ProjectId, "project_id");

            entity.Property(e => e.MilestoneId)
                .HasColumnType("int(11)")
                .HasColumnName("milestone_id");
            entity.Property(e => e.ClassId)
                .HasColumnType("int(11)")
                .HasColumnName("class_id");
            entity.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasColumnName("end_date");
            entity.Property(e => e.ProjectId)
                .HasColumnType("int(11)")
                .HasColumnName("project_id");
            entity.Property(e => e.StartedDate)
                .HasColumnType("datetime")
                .HasColumnName("started_date");
            entity.Property(e => e.MilestoneName)
            .HasMaxLength(45)
            .HasColumnName("milestone_name");
            entity.Property(e => e.Description)
            .HasMaxLength(50)
            .HasColumnName("description");

            entity.HasOne(d => d.Class).WithMany(p => p.Milestones)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("milestone_ibfk_1");

            entity.HasOne(d => d.Project).WithMany(p => p.Milestones)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("milestone_ibfk_2");
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasKey(e => e.ProjectId).HasName("PRIMARY");

            entity.ToTable("project");

            entity.HasIndex(e => e.ClassId, "class_id");

            entity.Property(e => e.ProjectId)
                .HasColumnType("int(11)")
                .HasColumnName("project_id");
            entity.Property(e => e.ClassId)
                .HasColumnType("int(11)")
                .HasColumnName("class_id");
            entity.Property(e => e.Description)
                .HasMaxLength(50)
                .HasColumnName("description");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.ProjectName)
                .HasMaxLength(30)
                .HasColumnName("project_name");
            entity.Property(e => e.StartedDate)
                .HasColumnType("date")
                .HasColumnName("started_date");

            entity.HasOne(d => d.Class).WithMany(p => p.Projects)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("project_ibfk_1");
        });

        modelBuilder.Entity<ProjectMember>(entity =>
        {
            entity.HasKey(e => e.ProjectId).HasName("PRIMARY");

            entity.ToTable("project_member");

            entity.HasIndex(e => e.MemberId, "member_id");

            entity.Property(e => e.ProjectId)
                .ValueGeneratedOnAdd()
                .HasColumnType("int(11)")
                .HasColumnName("project_id");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.IsLeader).HasColumnName("is_leader");
            entity.Property(e => e.MemberId)
                .HasColumnType("int(11)")
                .HasColumnName("member_id");

            entity.HasOne(d => d.Member).WithMany(p => p.ProjectMembers)
                .HasForeignKey(d => d.MemberId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("project_member_ibfk_1");

            entity.HasOne(d => d.Project).WithOne(p => p.ProjectMember)
                .HasForeignKey<ProjectMember>(d => d.ProjectId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("project_member_ibfk_2");
        });

        modelBuilder.Entity<Subject>(entity =>
        {
            entity.HasKey(e => e.SubjectId).HasName("PRIMARY");

            entity.ToTable("subject");

            entity.HasIndex(e => e.CreatedBy, "created_by");

            entity.HasIndex(e => e.ManagerId, "manager_id");

            entity.HasIndex(e => e.UpdatedBy, "updated_by");

            entity.Property(e => e.SubjectId)
                .HasColumnType("int(11)")
                .HasColumnName("subject_id");
            entity.Property(e => e.CreatedBy)
                .HasColumnType("int(11)")
                .HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("date")
                .HasColumnName("created_date");
            entity.Property(e => e.Description)
                .HasMaxLength(50)
                .HasColumnName("description");
            entity.Property(e => e.ManagerId)
                .HasColumnType("int(11)")
                .HasColumnName("manager_id");
            entity.Property(e => e.SubjectName)
                .HasMaxLength(30)
                .HasColumnName("subject_name");
            entity.Property(e => e.UpdatedBy)
                .HasColumnType("int(11)")
                .HasColumnName("updated_by");
            entity.Property(e => e.UpdatedDate)
                .HasColumnType("date")
                .HasColumnName("updated_date");

            entity.HasOne(d => d.CreatedByNavigation).WithMany(p => p.SubjectCreatedByNavigations)
                .HasForeignKey(d => d.CreatedBy)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("subject_ibfk_2");

            entity.HasOne(d => d.Manager).WithMany(p => p.SubjectManagers)
                .HasForeignKey(d => d.ManagerId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("subject_ibfk_1");

            entity.HasOne(d => d.UpdatedByNavigation).WithMany(p => p.SubjectUpdatedByNavigations)
                .HasForeignKey(d => d.UpdatedBy)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("subject_ibfk_3");
        });

        modelBuilder.Entity<SystemSetting>(entity =>
        {
            entity.HasKey(e => e.SettingId).HasName("PRIMARY");

            entity.ToTable("system_setting");

            entity.Property(e => e.SettingId)
                .HasColumnType("int(11)")
                .HasColumnName("setting_id");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.SettingName)
                .HasMaxLength(50)
                .HasColumnName("setting_name");
            entity.Property(e => e.Type)
                .HasMaxLength(20)
                .HasColumnName("type");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.UserId).HasName("PRIMARY");

            entity.ToTable("user");

            entity.HasIndex(e => e.RoleId, "role_id");

            entity.Property(e => e.UserId)
                .HasColumnType("int(11)")
                .HasColumnName("user_id");
            entity.Property(e => e.Avatar)
                .HasMaxLength(255)
                .HasColumnName("avatar");
            entity.Property(e => e.CreateDate)
                .HasColumnType("date")
                .HasColumnName("create_date");
            entity.Property(e => e.FullName)
                .HasMaxLength(40)
                .HasColumnName("full_name");
            entity.Property(e => e.Gmail)
                .HasMaxLength(50)
                .HasColumnName("gmail");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Password)
                .HasMaxLength(40)
                .HasColumnName("password");
            entity.Property(e => e.Phone)
                .HasColumnType("int(11)")
                .HasColumnName("phone");
            entity.Property(e => e.RoleId)
                .HasColumnType("int(11)")
                .HasColumnName("role_id");
            entity.Property(e => e.Username)
                .HasMaxLength(20)
                .HasColumnName("username");

            entity.HasOne(d => d.Role).WithMany(p => p.Users)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("user_ibfk_1");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    public List<SelectListItem> GetTeacherOptions()
    {
        var lstOption = Users.Include(x => x.Role)
            .Where(x => x.Role.Type == "role" && x.Role.SettingName == "teacher")
            .Select(a => new SelectListItem { Value = a.UserId.ToString(), Text = a.FullName.ToString() })
            .ToList();
        AddNullSelectOption(lstOption, "Teacher");
        return lstOption;
    }
    public List<SelectListItem> GetSemesterOptions()
    {
        var lstOption = SystemSettings
            .Where(x => x.Type == "semester")
            .Select(a => new SelectListItem { Value = a.SettingId.ToString(), Text = a.SettingName.ToString() })
            .ToList();
        AddNullSelectOption(lstOption, "Semester");
        return lstOption;
    }
    public List<SelectListItem> GetSubjectOptions()
    {
        var lstOption = Subjects
            .Select(a => new SelectListItem { Value = a.SubjectId.ToString(), Text = a.SubjectName.ToString() })
            .ToList();
        AddNullSelectOption(lstOption, "Subject");
        return lstOption;
    }

    public void AddNullSelectOption(List<SelectListItem> lstOption, string name)
    {
        lstOption.Insert(0, new SelectListItem { Value = null, Text = $"-- Select {name} --" });
    }
}
