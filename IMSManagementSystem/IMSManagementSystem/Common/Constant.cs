﻿namespace IMSManagementSystem.Commons
{
    public class Constant
    {
        public const int pageSize = 10;
        public const string classNameRequired = "Class Name is Required!";
        public const string subjectRequired = "Subject is Required!";
        public const string semesterRequired = "Semester is Required!";
        public const string teacherRequired = "Teacher is Required!";
        public const string startedDateRequired = "Started Date is Required!";
        public const string statusRequired = "Status is Required!";
    }
}
