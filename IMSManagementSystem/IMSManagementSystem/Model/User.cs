﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class User
{
    public int UserId { get; set; }

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string FullName { get; set; } = null!;

    public string Gmail { get; set; } = null!;

    public int Phone { get; set; }

    public string? Avatar { get; set; }

    public DateTime CreateDate { get; set; }

    public int? RoleId { get; set; }

    public bool IsActive { get; set; }

    public virtual ICollection<ClassStudent> ClassStudents { get; set; } = new List<ClassStudent>();

    public virtual ICollection<Class> Classes { get; set; } = new List<Class>();

    public virtual ICollection<Issue> IssueAssignees { get; set; } = new List<Issue>();

    public virtual ICollection<Issue> IssueAssigners { get; set; } = new List<Issue>();

    public virtual ICollection<ProjectMember> ProjectMembers { get; set; } = new List<ProjectMember>();

    public virtual SystemSetting? Role { get; set; }

    public virtual ICollection<Subject> SubjectCreatedByNavigations { get; set; } = new List<Subject>();

    public virtual ICollection<Subject> SubjectManagers { get; set; } = new List<Subject>();

    public virtual ICollection<Subject> SubjectUpdatedByNavigations { get; set; } = new List<Subject>();
}
