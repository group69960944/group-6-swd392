﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class ClassStudent
{
    public int ClassId { get; set; }

    public int StudentId { get; set; }

    public bool IsActive { get; set; }

    public virtual Class Class { get; set; } = null!;

    public virtual User Student { get; set; } = null!;
}
