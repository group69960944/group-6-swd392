﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class ProjectMember
{
    public int ProjectId { get; set; }

    public int MemberId { get; set; }
    public bool IsLeader { get; set; }

    public bool IsActive { get; set; }

    public virtual User Member { get; set; } = null!;

    public virtual Project Project { get; set; } = null!;
}
