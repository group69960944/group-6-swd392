﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class Issue
{
    public int IssueId { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public int ProjectId { get; set; }

    public int AssignerId { get; set; }

    public int? AssigneeId { get; set; }

    public int TypeId { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public virtual User? Assignee { get; set; }

    public virtual User Assigner { get; set; } = null!;

    public virtual Project Project { get; set; } = null!;
}
