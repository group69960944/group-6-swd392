﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class Milestone
{
    public int MilestoneId { get; set; }

    public int? ClassId { get; set; }

    public int? ProjectId { get; set; }
    public string? MilestoneName { get; set; }
    public string? Description { get; set; }

    public DateTime StartedDate { get; set; }

    public DateTime EndDate { get; set; }

    public virtual Class? Class { get; set; }

    public virtual Project? Project { get; set; }
}
