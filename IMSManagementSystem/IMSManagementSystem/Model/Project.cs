﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class Project
{
    public int ProjectId { get; set; }

    public int ClassId { get; set; }

    public string ProjectName { get; set; } = null!;

    public DateTime StartedDate { get; set; }

    public string? Description { get; set; }

    public bool IsActive { get; set; }

    public virtual Class Class { get; set; } = null!;

    public virtual ICollection<Issue> Issues { get; set; } = new List<Issue>();

    public virtual ICollection<Milestone> Milestones { get; set; } = new List<Milestone>();

    public virtual ProjectMember? ProjectMember { get; set; }
}
