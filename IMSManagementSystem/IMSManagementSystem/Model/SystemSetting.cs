﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class SystemSetting
{
    public int SettingId { get; set; }

    public string SettingName { get; set; } = null!;

    public bool IsActive { get; set; }

    public string Type { get; set; } = null!;

    public virtual ICollection<Class> Classes { get; set; } = new List<Class>();

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
