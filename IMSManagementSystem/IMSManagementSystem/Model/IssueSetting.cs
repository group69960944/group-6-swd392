﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class IssueSetting
{
    public int SettingId { get; set; }

    public string SettingName { get; set; } = null!;

    public bool IsActive { get; set; }
}
