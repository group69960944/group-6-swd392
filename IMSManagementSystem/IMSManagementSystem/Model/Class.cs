﻿using IMSManagementSystem.Commons;
using Org.BouncyCastle.Asn1.Cmp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace IMSManagementSystem.Models;
public partial class Class
{
    public int ClassId { get; set; }
    [Required(ErrorMessage = Constant.classNameRequired)]
    public string ClassName { get; set; } = null!;
    [Required(ErrorMessage = Constant.subjectRequired)]
    public int? SubjectId { get; set; }
    [Required(ErrorMessage = Constant.semesterRequired)]
    public int? SemesterId { get; set; }
    [Required(ErrorMessage = Constant.teacherRequired)]
    public int? TeacherId { get; set; }
    [Required(ErrorMessage = Constant.startedDateRequired)]
    public DateTime? StartedDate { get; set; }
    [Required(ErrorMessage = Constant.statusRequired)]
    public bool? IsPending { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public virtual ICollection<ClassStudent> ClassStudents { get; set; } = new List<ClassStudent>();

    public virtual ICollection<Milestone> Milestones { get; set; } = new List<Milestone>();

    public virtual ICollection<Project> Projects { get; set; } = new List<Project>();

    public virtual SystemSetting Semester { get; set; } = null!;

    public virtual Subject Subject { get; set; } = null!;

    public virtual User? Teacher { get; set; }
}
