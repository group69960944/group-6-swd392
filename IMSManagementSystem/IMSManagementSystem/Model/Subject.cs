﻿using System;
using System.Collections.Generic;

namespace IMSManagementSystem.Models;

public partial class Subject
{
    public int SubjectId { get; set; }

    public int ManagerId { get; set; }

    public string SubjectName { get; set; } = null!;

    public string? Description { get; set; }

    public DateTime CreatedDate { get; set; }

    public int CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public int UpdatedBy { get; set; }

    public virtual ICollection<Class> Classes { get; set; } = new List<Class>();

    public virtual User CreatedByNavigation { get; set; } = null!;

    public virtual User Manager { get; set; } = null!;

    public virtual User UpdatedByNavigation { get; set; } = null!;
}
